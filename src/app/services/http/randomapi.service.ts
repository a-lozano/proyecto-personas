import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { connections } from './../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class RandomapiService {
    constructor(private http: HttpClient) {}

    getRandomPersons() {
        return this.http.get(`${connections.randomapi}`);
    }
}
