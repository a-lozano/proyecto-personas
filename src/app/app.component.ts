import { Component, OnInit } from '@angular/core';

import { RandomapiService } from './services/http/randomapi.service';
import swal from 'sweetalert2';
import * as $ from 'jquery';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    title = 'proyecto-personas';

    persona = {
        nombre: '',
        edad: 18,
        sexo: '',
        documento: {
            tipo: '',
            nombre: '',
            data: '',
        },
    };

    personas: any[] = [];
    personasLocalStorage: any[] = [];

    constructor(private randomapiService: RandomapiService) {}

    ngOnInit() {
        this.personasLocalStorage = JSON.parse(
            localStorage.getItem('proyecto-personas.personas')
        );

        if (!this.personasLocalStorage) this.personasLocalStorage = [];

        this.personas = [...this.personasLocalStorage];

        this.randomapiService.getRandomPersons().subscribe(
            (res: any) => {
                const { results } = res;

                this.personas = this.personas.concat(results);
            },
            (error: any) => {
                swal.fire({
                    icon: 'error',
                    html: `${error.message}<br>${error.error}`,
                });
            }
        );
    }

    agregarArchivo() {
        const files = $('#fileField').prop('files');

        let $this = this;

        for (var i = 0, len = files.length; i < len; i++) {
            const file = files[i];

            const reader = new FileReader();

            reader.onload = (function (f) {
                return function (e) {
                    $this.persona.documento = {
                        tipo: f.type.split('/')[0],
                        nombre: f.name,
                        data: e.target.result,
                    };
                };
            })(file);

            reader.onerror = (function (f) {
                return function (e) {
                    swal.fire({
                        icon: 'error',
                        html: 'No fue posible agregar el archivo',
                    });
                };
            })(file);

            reader.readAsDataURL(file);
        }
    }

    guardarPersona() {
        if (!this.persona.nombre)
            return swal.fire({
                icon: 'error',
                html: 'Escribe un nombre para guardarlo en la tabla',
            });

        if (this.persona.edad <= 0)
            return swal.fire({
                icon: 'error',
                html: 'Escribe un número positivo',
            });

        if (!this.persona.sexo)
            return swal.fire({
                icon: 'error',
                html: 'Selecciona un sexo para agregar el registro',
            });

        this.personasLocalStorage.push(this.persona);

        localStorage.setItem(
            'proyecto-personas.personas',
            JSON.stringify(this.personasLocalStorage)
        );

        this.personas.push(this.persona);

        this.clearData();
    }

    clearData() {
        this.persona = {
            nombre: '',
            edad: 18,
            sexo: '',
            documento: {
                tipo: '',
                nombre: '',
                data: '',
            },
        };
    }

    clickInputFile() {
        $('#fileField').click();
    }
}
